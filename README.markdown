# Sketchfab for Confluence

Sketchfab for Confluence in an Atlassian Connect addon that allows you to include Sketchfab 3D models in
your Confluence pages as page macros. [Sketchfab][1] is an online service that allows 3D artists to upload
their models for everybody to view and share.

To read the documentation for this service please visit: https://sketchfab-connect.aerobatic.io/

To install this add-on please [visit the Atlassian Marketplace][2].

## Building Sketchfab for Cloud

To setup this project simply run the following from the command line:

    npm install

Now you will be able to perform local development or production development.

### Local Development

#### Starting Confluence locally

For local development you will want to start a locally running Confluence instance. Do the following:

    bash ./scripts/run-confluence.bash

That will start Confluence running in one tab and, shortly, it will be avaliable at http://localhost:1990/confluence

#### Running Sketchfab for Confluence locally

Since this is just a static add-on you can build the resources locally and run a local http server by doing the following:

    grunt build-local 
    grunt serve

This will build all of the resources for local development into the 'output' directory and then serve those resources via HTTP. You can find the descriptor at: http://localhost:8530/atlassian-connect.json

You can then install that Atlassian Connect descriptor into your locally running Confluence to test out this add-on locally.

### Production Updates

Sketchfab for Confluence is [deployed using Aerobatic][3] directly via bitbucket. I highly reccomend that you use this service. Thus to get your changes into production all you have to do is:

    grunt build-prod
    git ci -a
    git push origin master

This will build the production resources into the 'latest-production' directory and then push them to Bitbucket and automatically deploy them to Aerobatics online hosting. Very smooth!

 [1]: https://sketchfab.com/
 [2]: https://marketplace.atlassian.com/plugins/com.atlassian.confluence.sketchfab-connect/cloud/overview
 [3]: https://marketplace.atlassian.com/plugins/aerobatic-bitbucket-addon/cloud/overview