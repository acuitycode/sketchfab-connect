# Sketchfab for Confluence

Welcome to Sketchfab for Confluence. The best way to integrate Sketchfab into your Confluence pages.
Just install the addon and then paste Sketchfab models straight into Confluence for your entire team
to see. To see how it works just watch the following video:

<iframe width="640" height="360" src="https://www.youtube.com/embed/F2eEYScMavM" frameborder="0" allowfullscreen></iframe>

To install Sketchfab for Confluence please click on the install link above.
