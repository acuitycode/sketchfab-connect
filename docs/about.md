# About Sketchfab for Confluence

Sketchfab for Confluence was originally written in a single day by [Robert Massaioli][2]. The aim was to
bring great value to teams everywhere by letting them collaborate on their 3D models and animations.

Sketchfab for Confluence is actively monitored but is not under heavy development. However, if you
wish to see new features then please don't hesitate to [raise issues][1] and we'll respond promptly.

 [1]: https://bitbucket.org/robertmassaioli/sketchfab-connect/issues/new
 [2]: https://robertmassaioli.wordpress.com/
