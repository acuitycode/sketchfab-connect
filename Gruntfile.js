/*global module:false*/
module.exports = function(grunt) {
   var htmlminOptions = function(outdir) {
      var config = {
         options: {                                 // Target options 
            removeComments: true,
            collapseWhitespace: true
         }
      };

      config.files = {};
      config.files[outdir + '/static/html/render-sketchfab.html'] = 'static/html/render-sketchfab.html';
      config.files[outdir + '/index.html'] = 'static/html/index.html';

      return config;
   };

   var requirejsOptions = function(outdir) {
      return {
         options: {
            'appDir': 'static/js',
            'baseUrl': '.',
            'dir': outdir + '/static/js',
            'paths': {
               'moment': 'lib/moment',
               'mustache': 'lib/mustache',
               'marked': 'lib/marked',
               'underscore': 'lib/underscore',
               'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'
            },
            'shim': {
               'jquery': {
                  deps: [],
                  exports: '$'
               },
               'aui': {
                  'deps': ['jquery'],
                  'exports': 'AJS'
               }
            },
            'wrapShim': true,
            'modules': [
               {
                  'name': 'app/render-sketchfab'
               },
            ]
         }
      };
   };

   var imageminOptions = function(outdir) {
      return {
         files: [{
            expand: true,                  // Enable dynamic expansion 
            cwd: 'static/',                   // Src matches are relative to this path 
            src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match 
            dest: outdir + '/static'                  // Destination path prefix 
         }]
      };
   };

   var md2htmlOptions = function(outdir) {
      return {
         options: {
            layout: 'docs.layout.html',
            basePath: outdir,
            markedOptions: {
               gfm: false,
               langPrefix: 'code-'
            }
         },
         files: [{
            expand: true,
            cwd: 'docs',
            src: ['**/*.md'],
            dest: outdir + '/docs',
            ext: '.html'
         }]
      };
   };

   // Project configuration.
   grunt.initConfig({
      // Metadata.
      pkg: grunt.file.readJSON('package.json'),
      banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
         '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
         '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
         '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
         ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
      // Task configuration.
      concat: {
         options: {
            banner: '<%= banner %>',
            stripBanners: true
         },
         dist: {
            src: ['lib/<%= pkg.name %>.js'],
            dest: 'dist/<%= pkg.name %>.js'
         }
      },
      uglify: {
         options: {
            banner: '<%= banner %>'
         },
         dist: {
            src: '<%= concat.dist.dest %>',
            dest: 'dist/<%= pkg.name %>.min.js'
         }
      },
      jshint: {
         options: {
            curly: true,
            eqeqeq: true,
            immed: true,
            latedef: true,
            newcap: true,
            noarg: true,
            sub: true,
            undef: true,
            unused: true,
            boss: true,
            eqnull: true,
            browser: true,
            globals: {
               jQuery: true
            }
         },
         gruntfile: {
            src: 'Gruntfile.js'
         },
         lib_test: {
            src: ['lib/**/*.js', 'test/**/*.js']
         }
      },
      qunit: {
         files: ['test/**/*.html']
      },
      watch: {
         local: {
            files: ['descriptor/*.json', 'static/html/**/*.html', 'static/js/**/*.js', 'static/images/**/*', 'docs/*.md', 'docs.layout.html'],
            tasks: ['mustache_render:local', 'htmlmin:local', 'requirejs:local', 'imagemin:local', 'md2html:local']
         },
         gruntfile: {
            files: '<%= jshint.gruntfile.src %>',
            tasks: ['jshint:gruntfile']
         },
         lib_test: {
            files: '<%= jshint.lib_test.src %>',
            tasks: ['jshint:lib_test', 'qunit']
         }
      },
      'http-server': {
         'dev': {
            // the server root directory 
            root: 'output',

            // the server port 
            // can also be written as a function, e.g. 
            // port: function() { return 8282; } 
            port: 8530,

            // the host ip address 
            // If specified to, for example, "127.0.0.1" the server will 
            // only be available on that ip. 
            // Specify "0.0.0.0" to be available everywhere 
            host: "0.0.0.0",

            // Disable cache
            cache: -1,
            showDir : true,
            autoIndex: true,

            // server default file extension 
            ext: "html",

            // run in parallel with other tasks 
            runInBackground: false,
         }
      },
      mustache_render: {
         local: {
            files: [
               {
                  data: 'descriptor/local.json',
                  template: 'descriptor/atlassian-connect.json',
                  dest: 'output/atlassian-connect.json'
               }
            ]
         },
         prod: {
            files: [
               {
                  data: 'descriptor/prod.json',
                  template: 'descriptor/atlassian-connect.json',
                  dest: 'latest-production/atlassian-connect.json'
               }
            ]
         }
      },
      htmlmin: {                                     // Task 
         local: htmlminOptions('output'),
         prod: htmlminOptions('latest-production')
      },
      requirejs: {
         local: requirejsOptions('output'),
         prod: requirejsOptions('latest-production')
      },
      imagemin: {                          // Task 
         local: imageminOptions('output'),
         prod: imageminOptions('latest-production')
      },
      md2html: {
         local: md2htmlOptions('output'),
         prod: md2htmlOptions('latest-production')
      }
   });

   // These plugins provide necessary tasks.
   grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-contrib-uglify');
   grunt.loadNpmTasks('grunt-contrib-qunit');
   grunt.loadNpmTasks('grunt-contrib-jshint');
   grunt.loadNpmTasks('grunt-contrib-watch');
   grunt.loadNpmTasks('grunt-http-server');
   grunt.loadNpmTasks('grunt-mustache-render');
   grunt.loadNpmTasks('grunt-contrib-htmlmin');
   grunt.loadNpmTasks('grunt-contrib-requirejs');
   grunt.loadNpmTasks('grunt-contrib-imagemin');
   grunt.loadNpmTasks('grunt-md2html');

   // Default task.
   grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);
   grunt.registerTask('build-local', ['mustache_render:local', 'htmlmin:local', 'requirejs:local', 'imagemin:local', 'md2html:local']);
   grunt.registerTask('build-prod', ['mustache_render:prod', 'htmlmin:prod', 'requirejs:prod', 'imagemin:prod', 'md2html:prod']);
   grunt.registerTask('serve', ['http-server:dev']);
};
