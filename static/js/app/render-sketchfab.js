define(['../lib/URI', 'jquery'], function(URI, jquery) {
   "use strict";

   // Load the query params
   var queryParams = URI(window.location.href).query(true);

   // Load all.js
   var baseUrl = queryParams['xdm_e'] + queryParams['cp'];
   var apLoad = AJS.$.getScript(baseUrl + '/atlassian-connect/all.js');

   // Try to Load the model data
   var sketchfabUrl = queryParams['sketchfabUrl'];
   var sketchfabEmbedUrl = "https://sketchfab.com/oembed?url=" + queryParams['sketchfabUrl'];
   var inPreview = queryParams['outputType'] === 'preview';

   // Calculate the right dimensions
   var requestDimensions = function(w, h) { 
      return "&maxwidth=" + w + "&maxheight=" + h;
   }

   if(inPreview) {
      sketchfabEmbedUrl += requestDimensions(480, 360);
   } else {
      sketchfabEmbedUrl += requestDimensions(640, 480);
   }

   // Run the load request
   var getModel = AJS.$.get(sketchfabEmbedUrl);

   // A generic resize function
   var resize = function() {
      apLoad.done(function() {
         AP.resize();
      });
   };
   
   // Showing the model when done
   var useThumbnail = queryParams['useThumbnail'] !== undefined;
   getModel.done(function(data) {
      if(useThumbnail) {
         var img = AJS.$(document.createElement('img'));
         img.attr('src', data.thumbnail_url)
            .attr('width', data.thumbnail_width)
            .attr('height', data.thumbnail_height)
         AJS.$('body').empty().append(img);
      } else {
         AJS.$('body')
            .empty()
            .css('text-align', 'center')
            .append(data.html);
      }
      resize();
   });

   // Showing an error message when the model fails to load.
   getModel.fail(function() {
      AJS.$("#model-missing").removeClass("hidden");
      AJS.$("#model-missing a").attr('href', sketchfabUrl);
      resize();
   });
});
